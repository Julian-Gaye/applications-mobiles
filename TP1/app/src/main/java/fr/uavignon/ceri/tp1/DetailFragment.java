package fr.uavignon.ceri.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp1.data.Country;

public class DetailFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        TextView pays = view.findViewById(R.id.pays);
        ImageView drapeau = view.findViewById(R.id.drapeau);
        TextView capitale = view.findViewById(R.id.capitale);
        TextView langue = view.findViewById(R.id.langue);
        TextView monnaie = view.findViewById(R.id.monnaie);
        TextView population = view.findViewById(R.id.population);
        TextView superficie = view.findViewById(R.id.superficie);

        pays.setText(Country.countries[args.getCountryId()].getName());
        String uri = Country.countries[args.getCountryId()].getImgUri();
        Context c = drapeau.getContext();
        drapeau.setImageDrawable(c.getResources().getDrawable(
                c.getResources().getIdentifier(uri, null, c.getPackageName())));
        capitale.setText(Country.countries[args.getCountryId()].getCapital());
        langue.setText(Country.countries[args.getCountryId()].getLanguage());
        monnaie.setText(Country.countries[args.getCountryId()].getCurrency());
        population.setText(String.valueOf(Country.countries[args.getCountryId()].getPopulation()));
        superficie.setText(String.valueOf(Country.countries[args.getCountryId()].getArea()) + " km²");

        view.findViewById(R.id.button_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }
}