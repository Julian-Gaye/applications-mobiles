package fr.uavignon.ceri.tp1;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import fr.uavignon.ceri.tp1.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemPays.setText(Country.countries[i].getName());
        viewHolder.itemDetail.setText(Country.countries[i].getCapital());
        String uri = Country.countries[i].getImgUri();
        Context c = viewHolder.getContext();
        viewHolder.setImageDrawable(c.getResources().getDrawable(
                c.getResources().getIdentifier(uri, null, c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return Country.countries.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemDrapeau;
        TextView itemPays;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemDrapeau = itemView.findViewById(R.id.drapeau);
            itemPays = itemView.findViewById(R.id.pays);
            itemDetail = itemView.findViewById(R.id.detail);

            itemView.setOnClickListener(v -> {

                int position = getAdapterPosition();

                ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(position);
                Navigation.findNavController(v).navigate(action);
            });

        }

        public Context getContext() {
            return this.itemView.getContext();
        }

        public void setImageDrawable(Drawable drawable) {
            itemDrapeau.setImageDrawable(drawable);
        }
    }

}
